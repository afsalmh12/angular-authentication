import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserApiService } from 'src/app/core-modules/api/api-services/user-api-service';
import { IUploadResponse } from 'src/app/core-modules/interfaces/auth-interfaces';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  imageSelected: File
  uploadLoadProgess: number
  imagePath:string=undefined
  constructor(private api: UserApiService) { }

  ngOnInit(): void {
  }
  uploadPic = ($event: any) => {
    this.imageSelected = $event.target.files[0];
    if (this.imageSelected) {
      const formData = new FormData();
      formData.append('image', this.imageSelected)
      this.api.uploadPicture(formData).subscribe((res) => {
        if (res.type === HttpEventType.UploadProgress) {
          this.uploadLoadProgess = Math.round(100 * res.loaded / res.total);
        } else if (res.type == HttpEventType.Response) {
          this.uploadLoadProgess = undefined
          this.imageSelected = undefined
          this.imagePath = `${environment.assetsUrl}${(res.body as IUploadResponse).path}`
        }
      }, (err) => {
        this.uploadLoadProgess = undefined
      })
    }
  }

}
