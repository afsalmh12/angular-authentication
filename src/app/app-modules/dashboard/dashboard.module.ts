import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { DashbordComponent } from './dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';


@NgModule({
  declarations: [NavBarComponent,DashbordComponent, ProfileComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
