import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IfLoggedIn } from './core-modules/authentication/auth-gaurd-if-logged-in';
import { IfNotLoggedIn } from './core-modules/authentication/auth-gaurd-if-not-logged-in';

import { LoginComponent } from './general-components/login/login.component';
import { SignupComponent } from './general-components/signup/signup.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [IfNotLoggedIn]
  }, {
    path: 'signup',
    component: SignupComponent,
    canActivate: [IfNotLoggedIn]
  },
  {
    path: 'dashboard',
    canActivate:[IfLoggedIn],
    loadChildren: () => import('./app-modules/dashboard/dashboard-routing.module').then(m => m.DashboardRoutingModule),

  },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
