import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export const passwordMatchValidator: ValidatorFn = (
    control: FormGroup
  ): ValidationErrors | null => {
    if (control.get('password').value === control.get('confirm_password').value) {
      return null;
    } else {
      return { "notSame":"notSame"};
    }
  };
  