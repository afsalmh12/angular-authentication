import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, from } from 'rxjs';
import {  filter,  } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IUser } from '../interfaces/auth-interfaces';
import { UserApiService } from '../api/api-services/user-api-service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    loggedUer: number;

    private loginStatus: BehaviorSubject<boolean> = new BehaviorSubject(false);
    loginStatus$: Observable<boolean> = this.loginStatus.asObservable();

    public currentUser: BehaviorSubject<IUser> = new BehaviorSubject(undefined);
    currentUser$: Observable<IUser> = this.currentUser.asObservable().pipe(
        filter(value => Boolean(value))
    );

    constructor(
       private router:Router
    ) {
    }

    set token(token: string) {
        localStorage.setItem('token', token);
        this.loginStatus.next(true)
    }

    get token(): string {
        return localStorage.getItem('token')
    }

    public intialize() {
        if (this.token) {
            console.log("Already logged user")
            this.loginStatus.next(true)
        }
    }



    private purgeToken() {
        localStorage.removeItem('token');
    }

    logout() {
        this.currentUser.next(undefined)
        this.purgeToken()
        this.loginStatus.next(false);
        this.router.navigate(['/login'])
    }

}
