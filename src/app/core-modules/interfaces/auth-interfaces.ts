export interface IUser{
    email : string
    password: string
}
export interface ILogin{
    email : string
    password: string
}

export interface IToken{
    token:string
}

export interface IUploadResponse{
    message:string,
    path:string
}