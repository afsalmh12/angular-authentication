
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { apiRef } from '../api-end-points/api-end-points';

export class ApiServiceBase {


    constructor(
        private http: HttpClient
    ) {
    }

    private _apiHttpRequest(
        httpMethodName: string,
        apiName: string,
        body?: object | FormData,
        urlParams?: Array<any>,
        queryParams?: object,
        options?: object
    ): Observable<any> {
        const actionUrl = getApiUrl(apiName, urlParams, queryParams);
        console.log(`Building request for: ${apiName}`);

        if (['get', 'delete'].includes(httpMethodName)) {
            // either get or delete. no body required
            return this.http[httpMethodName](actionUrl, options);
        } else {
            // body required
            return this.http[httpMethodName](actionUrl, body, options);
        }
    }

    // GET:
    public _apiRead(apiName, urlParams?, queryParams?: object, options?) {
        return this._apiHttpRequest('get', apiName, null, urlParams, queryParams, options);
    }

    // POST:
    public _apiCreate(apiName, body, urlParams?, options?) {
        return this._apiHttpRequest('post', apiName, body, urlParams, null, options);
    }

    // PUT
    public _apiUpdate(apiName, body, urlParams?, options?) {
        return this._apiHttpRequest('put', apiName, body, urlParams, null, options);
    }

    // PATCH
    public _apiPartialUpdate(apiName, body, urlParams?, options?) {
        return this._apiHttpRequest('patch', apiName, body, urlParams, null, options);
    }

    // DELETE
    public _apiDelete(apiName, urlParams?, options?) {
        return this._apiHttpRequest('delete', apiName, null, urlParams, null, options);
    }






}
export const formatIndexedString = (pattern: string, paramSequence: Array<any>): string => {
    if (paramSequence) {
        for (let i = 0; i < paramSequence.length; i++) {
            const replaceTag = '{' + (i + 1) + '}';
            pattern = pattern.replace(replaceTag, paramSequence[i] || '');
        }
    }
    return pattern;
}



export const getApiUrl = (apiName: any, urlParams?: Array<any>, queryParams?: object): string => {
    let apiUrl = apiName
    let endPoint: string;
    if (urlParams && urlParams.length) {
        apiUrl = formatIndexedString(apiUrl, urlParams);
    }
    if (queryParams) {
        const allQueries = Object.keys(queryParams);
        if (allQueries.length) {
            apiUrl += '?';
            allQueries.forEach(param => {
                const queryValue = queryParams[param];
                if (!['', undefined, null].includes(queryValue)) {
                    apiUrl = `${apiUrl}${param}=${queryValue}&`;
                }
            });
        }
    }
    endPoint = environment.baseUrl + apiUrl;
    return endPoint;
}
