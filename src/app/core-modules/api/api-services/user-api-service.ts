import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiServiceBase } from '../api-base/api-base';
import { ILogin, IToken, IUser } from '../../interfaces/auth-interfaces';
import { apiRef } from '../api-end-points/api-end-points';

@Injectable({
    providedIn: 'root'
  })
  export class UserApiService extends ApiServiceBase {
    constructor(http: HttpClient) {
      super(http)
    }

    signUp = (data:IUser):Observable<IUser> => {
        return this._apiCreate(apiRef.singUp,data)
    }
    login = (data:ILogin):Observable<IToken> => {
        return this._apiCreate(apiRef.login,data)
    }
    uploadPicture(body:FormData):Observable<any>{
      const options = {
        reportProgress : true,
        observe: 'events'
      }
      return this._apiCreate(apiRef.image,body,null,options)
    }
   
  
}