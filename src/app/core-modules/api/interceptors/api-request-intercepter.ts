import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor, HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../../authentication/auth-service';



@Injectable()
export class ApiRequestInterceptor implements HttpInterceptor {
    constructor(
        private auth: AuthService,
    ) { }
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        let customHeaders = request.headers;
        if (request.url) {
            const url = request.url
            if (!(request.body instanceof FormData)) {
                customHeaders = customHeaders.set('Content-Type', 'application/json');

            }
            const updatedRequest = request.clone({
                headers: this.buildHeaderToken(customHeaders, url)
            });
            return next.handle(updatedRequest)
        }
    }

    private buildHeaderToken(headers: HttpHeaders, url: string): HttpHeaders {
        if (this.auth.token) {
            headers = headers.set('Authorization', this.auth.token);
        }
        return headers;
    }
}
