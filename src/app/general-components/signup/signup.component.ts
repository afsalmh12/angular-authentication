import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserApiService } from 'src/app/core-modules/api/api-services/user-api-service';
import { AuthService } from 'src/app/core-modules/authentication/auth-service';
import { passwordMatchValidator } from 'src/app/shared/validators/password-match-validator';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private api: UserApiService,
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.auth.loginStatus$.subscribe((loggedIn) => {
      if (loggedIn) {
        this.router.navigate(['/dashboard/profile'])
      }
    })
    this.signUpForm = this.formBuilder.group({
      email: ['', [Validators.required,
      Validators.email]],
      password: ['', [Validators.required]],
      confirm_password: ['', Validators.required],
    }, { validators: passwordMatchValidator });
  }
  get fControls() {
    return this.signUpForm.controls
  }

  signup = () => {
    if (this.signUpForm.valid) {
      delete this.signUpForm.value.confirm_password
      this.api.signUp(this.signUpForm.value).subscribe((result) => {
        this.router.navigate(['/login'])
      },(err) => {
        alert("Something went wrong")
      })
    }
  }
}
