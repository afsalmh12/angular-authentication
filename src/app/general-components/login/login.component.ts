import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserApiService } from 'src/app/core-modules/api/api-services/user-api-service';
import { AuthService } from 'src/app/core-modules/authentication/auth-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private api:UserApiService,
    private auth:AuthService,
    private router: Router,
    ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required,
      Validators.email]],
      password: ['', [Validators.required]],
    });

    this.auth.loginStatus$.subscribe((loggedIn) => {
      if(loggedIn){
        this.router.navigate(['/dashboard/profile'])
      }
    })
  }
  login = () =>{
    if(this.loginForm.valid){
      this.api.login(this.loginForm.value).subscribe((response) => {
        this.auth.token = response.token
        this.router.navigate(['/dashboard/profile'])
      },(err) => {
        alert("Something went wrong")
      })
    }
  }

  get fControls() {
    return this.loginForm.controls
  }
}
