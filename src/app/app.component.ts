import { Component, OnInit } from '@angular/core';
import { AuthService } from './core-modules/authentication/auth-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'cavli-wireless';
  constructor(   private auth: AuthService){
    
  }
  ngOnInit(): void {
    this.auth.intialize();
  }
}
