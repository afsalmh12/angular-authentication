export const environment = {
  production: true,
  baseUrl:"https://serene-hollows-11661.herokuapp.com/api/v1",
  assetsUrl:"https://serene-hollows-11661.herokuapp.com/"
};
